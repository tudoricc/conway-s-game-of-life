#include <stdio.h>
#include <omp.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int numberOfNeighbours(int line,int collumn,int **harta);
int** bordareToroid(int noLines,int noCollumns,int **harta);


int main(int argc , char *argv[] ){

  int noThreads,noSteps;
  char* nameInput =(char*) calloc(15,sizeof(char*)) ;
  char* nameOutput =(char*) calloc(15,sizeof(char*));
  FILE* file_input , *file_output ; 
  char *typeMap = (char*) calloc(1,sizeof(char*));
  int Wharta , Hharta , W , H,j,i, lineMax , colMax ;
  char *tok = NULL;
  char *firstLine ;
  int ** harta ;
  

  if (argc != 5){
    printf("Usage ./$exec-name $number-of-threads $number-of-steps $input-file $output-file");
  }

  //printf("ce mai faci");
  noThreads = atoi(argv[1]) ;
  noSteps = atoi(argv[2]);
  
  nameInput = argv[3];
  nameOutput = argv[4];

  file_input = fopen(nameInput , "r");
  file_output = fopen(nameOutput , "w");

  if (file_input == NULL) {
    fprintf(stderr, "Can't open input file %s!\n", nameInput);
    exit(1);
  }
//  rewind(file_input);
  //forma fisierului de input este $TypeMap $latimeHarta $lungimeHarta $latimeHartaSimulat $lungimeHartaSimulat

  
//  rewind(file_input);
  fscanf(file_input,"%c %d %d %d %d",typeMap , &Wharta,&Hharta,&W,&H);

  omp_set_num_threads(noThreads);

  if (strcmp(typeMap,"P") == 0){
  //  printf("SDasd\n");
    //reprezentare in plan
     int pasi ;

   //   /*aloc +2 pentru ca pastrez pe tot parcursul distractiei matricea direct bordata pentru ca 
   //   nu am motive sa bordez si debordez*/
      harta = (int**) calloc(H+2,sizeof(int*));
  
    
  //    //TODO:Paralelizeaza aici ca nu e vreo problema
  //    //fac bordarea din prima
      int k = 0;
      while (k<H+2){
        harta[k] = (int*) calloc (W+2,sizeof(int));
        k+=1;
} 
   
    int m,n; 

 int **harta2 = (int**) calloc(Hharta+2,sizeof(int*));
  
    
  //    //TODO:Paralelizeaza aici ca nu e vreo problema
  //    //fac bordarea din prima
      k = 0;
      while (k<Hharta+2){
        harta2[k] = (int*) calloc (Wharta+2,sizeof(int));
        k+=1;
} 


    if(H>=Hharta ){
      if(W>=Wharta){
      for (m = 1  ; m <= Hharta ; ++m){
        for (n = 1 ; n <= Wharta ; ++n) {
          fscanf(file_input,"%d",&harta[m][n]);
        }
      }

    }
  }
  else{
     for (m = 1  ; m <= Hharta ; ++m){
        for (n = 1 ; n <= Wharta ; ++n) {
          fscanf(file_input,"%d",&harta2[m][n]);
        }
      }

      for ( m = 1 ; m <= H ; m++){
        for ( n = 1 ; n<= W ; n++){
          harta[m][n] = harta2[m][n];
        }
      }

      /*for (m = 0 ; m< H+2 ; m++){
        for ( n = 0 ; n < W+2 ; n++){
          printf("%d ",harta[m][n]);
        }
        printf("\n");
      }*/
  }

 
      


      int **hartaNoua = (int**) calloc(H+2,sizeof(int*));
      k=0;
      while (k<H+2){
        hartaNoua[k] = (int*) calloc (W+2,sizeof(int));
        k+=1;
} 
 //    //aici incepe prelucrarea propriu zisa pe etape
     
      for (pasi = 0 ; pasi < noSteps ; pasi++){
      //  printf("intra in for");
         
        //TODO:ai putea paraleliza si aici,nu UITA
        int neighbours;

        #pragma omp parallel for private(i,j,neighbours) 
        for (i = 1 ; i < H+1 ; i++){
          for (j = 1 ; j < W+1 ; j++){
            neighbours = numberOfNeighbours(i,j,harta);
            //printf("%d\n",neighbours);
            if (neighbours == 3){
              hartaNoua[i][j] = 1 ;
            }
            else if (neighbours == 2){
              hartaNoua[i][j] = harta[i][j];
            }
            else{
                hartaNoua[i][j] = 0;
            }
          }
        }

      //free(harta);
//      memcpy(harta,hartaNoua,(H+2)*(W+2)*sizeof(int));
      int value;
      

      
      
      #pragma omp parallel for private(i,j,value)
      for (i = 0 ; i< H + 2 ; i++ ){
        
        for ( j = 0 ; j < W+2 ; j++) {
          value = hartaNoua[i][j];
          harta[i][j] = hartaNoua[i][j];
        }
      }
   
    }
   

      lineMax = 0;
      colMax = 0;
      //vezi ca aici s-ar putea sa iti futa ceva
      //REMINDER : Poti sa te uiti aici daca nu iti dau timpii prea ok
      
      #pragma omp parallel for private(i,j)
      for (i = 1; i <= H ; i++) {
        for (j = 1 ; j <= W ; j++){
          if (harta[i][j] == 1){
            if (lineMax <= i){
              lineMax = i;
            }
            
          }
        }
      }
      #pragma omp parallel for private(i,j)
       for (i = 1; i <= H ; i++) {
        for (j = 1 ; j <= W ; j++){
          if (harta[i][j] == 1){
            if (colMax <= j){

              colMax = j;
            }
          }
         }
        }
      
/*      for (j = 0 ; j <= W - 1; j++){
        for ( i = 0 ; i< H  ++i){
          if (harta[i+1][j+1] == 1) {
            colMax = j+1      ;
          }
        }
      }
*/
  //    lineMax = i+1;
//      colMax = j+1;
      //printf("Linia maxima %d si coloana maxima %d",lineMax , colMax);
      //free(hartaNoua);

      fprintf(file_output,"%s %d %d %d %d\n",typeMap,colMax,lineMax,W,H);

      if (lineMax != 0 && colMax != 0){
        for (i = 1  ; i < lineMax+1 ; i++){
          for (j = 1 ; j < colMax+1 ; j++) {
            fprintf(file_output,"%d ",harta[i][j]);
          }
         fprintf(file_output,"\n");
        }
      }
  }
  else if (strcmp(typeMap,"T") == 0){
    //reprezentare in toroid
    //printf("Intra aici");
    int pasi ;
  /*  in cazul asta nu are rost sa aloci o dimensiune mai mare
    si faci matricea de WxH si la fiecare pas faci acea bordare specifica*/
    harta = (int**) calloc(H,sizeof(int*));

    int k = 0;
    while(k<H){
      harta[k] = (int*) calloc (W,sizeof(int));
      k+=1;
    }
    
      for (i = 0  ; i < Hharta ; i++){
        for (j = 0 ; j < Wharta ; j++) {
          fscanf(file_input,"%d",&harta[i][j]);
        }
      }
      int **hartaNoua = (int**) calloc(H+2,sizeof(int*));

      k = 0;
      while(k<H+2){
      hartaNoua[k] = (int*) calloc (W+2,sizeof(int));
      k+=1;
    }
    
      int **hartaNoua2 = (int**) calloc(H+2,sizeof(int*));

      k = 0;
      while(k<H+2){
        hartaNoua2[k] = (int*) calloc (W+2,sizeof(int));
        k+=1;
      }
    
      //acum incep iteratiile 
      for (pasi =0 ; pasi < noSteps ; pasi ++ ){
        //int j = 0;
        hartaNoua = bordareToroid(H,W,harta);

        //TODO:SI aici ai putea modifica sa pui paralelizarea
        #pragma omp parallel for private(i,j)
        for (i = 1 ; i < H+1; i++){
          for ( j = 1 ; j < W+1 ; j++){

            int neighbours = numberOfNeighbours(i,j,hartaNoua); 
            if (neighbours == 3){
              harta[i-1][j-1] = 1 ;
            }
            else if (neighbours == 2){
              harta[i-1][j-1] = hartaNoua[i][j];
            }
            else {
            //| neighbours == 0 || neighbours > 3 ){
              harta[i-1][j-1] = 0;
            }
          }
        }
        /*for (i = 0 ; i < H+2; i++){
          for ( j = 0 ; j < W+2 ; j++){
            printf("%d ",hartaNoua[i][j]);
          }
          printf("\n");
        }*/


      }
      lineMax = 0;
      colMax = 0;
      #pragma omp parallel for private(i,j)
      for (i = 0 ; i < H ; i++) {
        for (j = 0 ; j < W ; j++){
          if (harta[i][j] == 1){
            if (lineMax <= i){
            lineMax = i;
          }
           
          }
        }
      }
       #pragma omp parallel for private(i,j)
      for (i = 0 ; i < H ; i++) {
        for (j = 0 ; j < W ; j++){
          if (harta[i][j] == 1){
           
            if (colMax <= j){

            colMax = j;
          }
          }
        }
      }
      /*printf("Linia maxima este %d si coloana maxima %d",lineMax+1,colMax+1);
    for (i = 0 ; i < H ; i++) {
        for (j = 0 ; j < W ; j++){
          printf("%d ",harta[i][j]);
        }
        printf("\n");
      }*/

      fprintf(file_output,"%s %d %d %d %d\n",typeMap,colMax+1,lineMax+1,W,H);

      for (i = 0  ; i < lineMax+1 ; i++){
        for (j = 0 ; j < colMax+1 ; j++) {
          fprintf(file_output,"%d ",harta[i][j]);
        }
        fprintf(file_output,"\n");
      }



  }

  fclose(file_input);
  fclose(file_output);
  return 1;
}

//functie pentru numarul de vecini
int numberOfNeighbours(int line,int collumn,int **harta){
  //
  int number = 0;
 
  if (harta[line][collumn-1] == 1){
    number+=1;
  }
  if (harta[line][collumn+1] == 1){
    number+=1;
  }
  if (harta[line-1][collumn] == 1){
    number+=1;
  }
  if (harta[line-1][collumn-1] == 1){
    number+=1;
  }
  if (harta[line-1][collumn+1] == 1){
    number+=1;
  }
  if (harta[line+1][collumn] == 1){
    number+=1;
  }
  if (harta[line+1][collumn-1] == 1){
    number+=1;
  }
  if (harta[line+1][collumn+1] == 1){
    number+=1;
  }

  return number;
}
int** bordareToroid(int noLines,int noCollumns,int **harta){
  int **matriceBordata = (int**) calloc(noLines+2,sizeof(int*));
    int i,j;
    for ( i = 0 ; i<noLines+2 ; i++){
      matriceBordata[i] = (int*) calloc (noCollumns+2,sizeof(int));
    }

    #pragma omp parallel for private(i,j) shared(harta)
    for (i = 1 ; i<=noLines; i++){
      for (j =1 ; j <=noCollumns; j++){
        matriceBordata[i][j] = harta[i-1][j-1];
      }
    }
    /*acum fac adevarata bordare specifica toroidului
       a b c       i g h i g
       d e f   ->  c a b c a
       g h i       f d e f d
                   i g h i g
           c a b c a
                   */

    matriceBordata[0][0] = harta[noLines-1][noCollumns-1];
    matriceBordata[0][noCollumns+1] = harta[noLines-1][0];
    matriceBordata[noLines+1][0] = harta[0][noCollumns-1];
    matriceBordata[noLines+1][noCollumns+1] = harta[0][0]; 
    #pragma omp parallel for private(i)
    for (i =  1 ; i <=noCollumns ; i++){
      matriceBordata[0][i] = harta[noLines-1][i-1];
    }
    #pragma omp parallel for private(j)
    for (j = 1 ; j <= noCollumns ; j++){
      matriceBordata[noLines+1][j] = harta[0][j-1];
    }
     #pragma omp parallel for private(j)
    for ( j = 1 ; j <= noLines ; j++ ){
//      for (i = 0 ; i <= noLines ; i = i+noLines){
        matriceBordata[j][0] = harta[j-1][noCollumns-1];
        matriceBordata[j][noCollumns+1] = harta[j-1][0];

  //    }
    }

    return matriceBordata;

}